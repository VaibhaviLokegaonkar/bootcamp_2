package tests;

import org.junit.Assert;
import org.junit.Test;

import src.Odometer;

public class OdometerTest {
    @Test
    public void isAscendingTest() {
    	Odometer odo = new Odometer(6);
    	Assert.assertEquals(true, odo.isAscending(123456));
    }
}
