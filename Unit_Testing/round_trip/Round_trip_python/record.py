from person import Person 

class Record():
    def __init__(self):
        self.register = dict()

    def add(self, name, start, end):
        if name not in self.register:
            self.register[name] = Person(start)
            return 
        self.register[name].update_completion_status(end)

    def no_of_round_completers(self):
        return sum(int(self.register[name].round_completed) for name in self.register)