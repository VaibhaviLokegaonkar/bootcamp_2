*Note: This doc is only the initial work for the Interval API. Actual definitions vary.*

# Range class
This class models a range with a start and an end point.

## Pre-initialisations
a -> int; b -> int; c -> int;
A -> range obj B -> range obj

## Initialising a range
Range() -> (0, INT_MAX)
Range(a) -> [0, a) or if a is negative [a, 0)
Range(a, b) -> [a, b)
Range(a, b, c) -> [a, b) with a_i = a_(i - 1) + c
Range(A) -> new Range obj deep copied.

## Operations on a range
- A.isOverlap(B) -> returns true if A and B have common elements.
- A.add(B) -> returns another Range C which adds ranges A and B with removed repetitions
- A.modify(a) -> changes the end point of A to a
- A.modify(a, b) -> changes the start and end point of A to a and b respectively
- A.sum() -> returns the sum of a range
- A.contains(a) -> returns true if A contains a
- A.getSet() -> returns all the values in the range A
- A.hasNegatives() -> returns true if it contains some negative elements
- A.maxima(function) -> returns the max value in the range for the given function
- A.minima(function) -> returns the minima of the function in A
- A.contains(B) -> returns true if B contains A
- A.touching(B) -> returns true if A.end == B.start
- A.greaterThan(B) -> ????
- A.isSubrangeOf(B) -> 
