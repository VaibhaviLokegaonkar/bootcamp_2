/* ----------------------------------------------------*
 * Represents the closed interval [a, b] where a, b    *
 * are integers.                                       *
 * ----------------------------------------------------*/
import java.util.*;
public class _Range {

    protected int lo;
    protected int hi;

    public _Range() {
        this.lo = this.hi = 0;
    }

    public _Range(int e) {
        this.lo = 0;
        this.hi = e;
    }

    public _Range(int a, int b) {
        this.lo = Math.min(a, b);
        this.hi = Math.max(a, b);
    }

    private void reset() {
        this.lo = this.hi = 0;
    }

    public void rStretch() {
        this.hi++;
    }

    public void rStretch(int n) {
        this.hi += n;
    }

    public void lStretch() {
        this.lo--;
    }

    public void lStretch(int n) {
        this.hi -= n;
    }

    public void stretch() {
        this.lo--;
        this.hi++;
    }

    public void stretch(int n) {
        this.lo -= n;
        this.hi += n;
    }

    public void squeeze() {
        this.lo++;
        this.hi--;
        if (this.lo > this.hi)
            this.reset();
    }

    public void squeeze(int n) {
        this.lo += n;
        this.hi -= n;
        if (this.lo > this.hi)
            this.reset();
    }

    public void shift(int n) {
        this.lo += n;
        this.hi += n;
    }
    
    public int length() {
        return this.hi - this.lo + 1;
    }
    
    public String toString() {
        return "[" + this.lo + ", " + this.hi + "]";
    }

    public boolean contains(int x) {
        return this.lo <= x && x <= this.hi;
    }
    public boolean contains(_Range r) {
        return this.lo <= r.lo && r.hi <= this.hi;
    }
    public boolean equals(_Range r) {
        return this.lo == r.lo && this.hi == r.hi;
    }
    public boolean isDisjoint(_Range r) {
        return this.lo > r.hi || this.hi < r.lo;
    }
    public boolean isOverlapping(_Range r) {
        return !(this.isDisjoint(r));
    } 
    public boolean lessThan(_Range r) {
        return this.lo < r.lo;
    }
    public enum Relation {
        SUBSET, SUPERSET, OVERLAPL, OVERLAPR, 
        TOUCHINGL, TOUCHINGR, LESSDISJOINT, MOREDISJOINT,
        SAME;
    }
    public Relation classify(_Range r) {
        if (this.hi == r.lo) 
            return Relation.TOUCHINGR;
        if (this.lo == r.hi)
            return Relation.TOUCHINGL;
        if (this.equals(r))
            return Relation.SAME;
        if (this.contains(r)) 
            return Relation.SUPERSET;
        if (r.contains(this))
            return Relation.SUBSET;
        if (this.isDisjoint(r))
            if (this.lo > r.hi)
                return Relation.MOREDISJOINT;
            else
                return Relation.LESSDISJOINT;
        if (this.lessThan(r))
            return Relation.OVERLAPL;
        return Relation.OVERLAPR;
    }

    public _Range merge(_Range r) {
        if (this.isDisjoint(r)) {
            return new _Range();
        }
        int a = Math.min(this.lo, r.lo);
        int b = Math.max(this.hi, r.hi);
        return new _Range(a, b);
    }            

    public _Range common(_Range r) {
        if (this.isDisjoint(r)) {
            return new _Range();
        }

        ArrayList<Integer> es = new ArrayList<Integer>();
        es.add(this.lo);
        es.add(this.hi);
        es.add(r.lo);
        es.add(r.hi);
        Collections.sort(es);
        return new _Range(es.get(1), es.get(2));
	//return _Range(sorted([self.lo, self.hi, r.lo, r.hi])[1:3])
    }
}
