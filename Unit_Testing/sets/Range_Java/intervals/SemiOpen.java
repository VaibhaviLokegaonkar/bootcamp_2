package intervals;

import intervals.Interval.Relation;

public class SemiOpen extends Interval {

	public SemiOpen() {
		super();
	}

	public SemiOpen(int end) throws Exception {
		super(end);
	}

	public SemiOpen(int start, int end) throws Exception {
		super(start, end);
	}
	
	@Override
	public int length() {
		if (this.high == Integer.MAX_VALUE) {
			return Integer.MAX_VALUE;
		}
		return this.high - this.low;
	}

	@Override
	public boolean contains(int x) {
		return this.low <= x && x < this.high;
	}

	@Override
	public boolean contains(Interval i) {
		return this.low <= i.low && i.high < this.high; 
	}

	@Override
	public boolean isDisjoint(Interval i) {
		return this.high <= i.low || i.high <= this.low;
	}

	public String toString() {
		return "[" + super.toString() + ")";
	}
	
	@Override
	public Relation classify(Interval i) {
		if (this.high - 1 == i.low) 
            return Relation.TOUCHINGR;
        if (this.low == i.high - 1)
            return Relation.TOUCHINGL;
        if (this.equals(i))
            return Relation.SAME;
        if (this.contains(i)) 
            return Relation.SUPERSET;
        if (i.contains(this))
            return Relation.SUBSET;
        if (this.isDisjoint(i))
            if (this.low > i.high)
                return Relation.MOREDISJOINT;
            else
                return Relation.LESSDISJOINT;
        if (this.lessThan(i))
            return Relation.OVERLAPL;
        return Relation.OVERLAPR;
	}

}
