package intervals;

public class Closed extends Interval {
	public Closed() {
		super();
	}
	
	public Closed(int high) throws Exception {
		super(high);
	}
	
	public Closed(int low, int high) throws Exception {
		super(low, high);
	}
	
	public int length() {
		return this.high - this.low + 1;
	}
	
	public boolean contains(int x) {
		return this.low <= x && x <= this.high;
	}
	
	public boolean contains(Interval i) {
		return this.low <= i.low && i.high <= this.high;
	}
	
	public boolean isDisjoint(Interval i) {
		return this.low > i.high || i.low > this.high;
	}
	
	public Relation classify(Interval i) {
		if (this.high == i.low) 
            return Relation.TOUCHINGR;
        if (this.low == i.high)
            return Relation.TOUCHINGL;
        if (this.equals(i))
            return Relation.SAME;
        if (this.contains(i)) 
            return Relation.SUPERSET;
        if (i.contains(this))
            return Relation.SUBSET;
        if (this.isDisjoint(i))
            if (this.low > i.high)
                return Relation.MOREDISJOINT;
            else
                return Relation.LESSDISJOINT;
        if (this.lessThan(i))
            return Relation.OVERLAPL;
        return Relation.OVERLAPR;
	}
	
	@Override
	public String toString() {
		return "[" + super.toString() + "]";
	}
}
