class Interval():
    def __init__(self, _start = 0, _end = 0):
        """
        This creates interval starting from start and ending at end.
        """
        self.start = min(_start, _end)
        self.end = max(_start, _end)

    def __repr__(self):
        if self.start == self.end:
            return "[" + str(self.start) + "]"   
        return "[" + str(self.start) + ", " + str(self.end) + "]"

    def reset(self):
        """
        This resets the start and ending of interval to 0.
        """
        self.start = 0
        self.end = 0

    def stretching(self, stretch_by = 1, direction = "both"):
        """
        This stretches the interval by 'stretch_by' (1 being default) in a direction specified 
        left, right or both (both being default).
        """
        def left():
            self.start -= stretch_by

        def right():
            self.end += stretch_by

        def both():
            self.start -= stretch_by
            self.end += stretch_by


        {"left": left, "right": right, "both": both}[direction]()
        if self.start > self.end:
            self.reset()

    def squeezing(self, squeeze_by = 1, direction = "both"):
        """
        This squeezes the interval by 'squeeze_by' (1 being default) in a direction specified
        left, right or both (both being default).
        """
        def negate(n):
            return -n

        self.stretching(negate(squeeze_by), direction)

    def shift(self, shift_by = 1, into = "front"):
        """
        This shifts the interval by 'shift_by' (1 being default) in a direction specified
        front or back (back being default).
        """
        def front():
            self.start += shift_by
            self.end += shift_by

        def back():
            self.start -= shift_by
            self.end -= shift_by

        {"front": front, "back": back}[into]()

    def __len__(self):
        """
        Returns the no. of integers in the current range.
        """
        return self.end - self.start + 1

    def contains(self, n):
        """
        Checks if the range contains the no. n.
        """
        return self.start <= n and n <= self.end

    def is_sub_interval(self, interval):
        """
        Checks if the given interval lies in the current interval. 
        """
        if self.equals(interval):
            return False
        return self.start <= interval.start and interval.end <= self.end

    def is_disjoint(self, interval):
        """
        Checks if the current and given interval have no element in common.
        """
        return self.end < interval.start or interval.end < self.start

    def is_overlapping(self, interval):
        """
        Checks if the current and given interval overlap.
        """
        return not self.is_disjoint(interval)
    
    def classify(self, interval):
        """
        Checks and returns the status of the interval and current object.
        """
        if self.end == interval.start:
            return "TOUCHING RIGHT"

        if self.start == interval.end:
            return "TOUCHING LEFT"

        if self.equals(interval):
            return "SAME"

        if self.is_sub_interval(interval):
            return "SUB-INTERVAL"

        if interval.is_sub_interval(self):
            return "SUPER-INTERVAL"    

        if self.is_disjoint(interval):
            if interval.end < self.start:
                return "MORE DISJOINT"
            return "LESS DISJOINT"

        if self.less_than(interval):
            return "LEFT OVERLAP"
        return "RIGHT OVERLAP"

    def merge(self, interval):
        """
        Merges the 2 intervals to give a new interval which includes the contents of both
        intervals.
        """
        if self.is_disjoint(interval):
            return Interval()

        return Interval(min(self.start, interval.start), max(self.end, interval.end))

    def common(self, interval):
        """
        Returns an interval which is present in both the current and given interval.
        """
        start, end = sorted([self.start, self.end, interval.start, interval.end])[1:3]
        return Interval(start, end)

    def equals(self, interval):
        """
        Checks whether both the intervals are equal or not.
        """
        return self.start == interval.start and self.end == interval.end

    def less_than(self, interval):
        """
        Checks whether the current interval is less than the given interval.
        """
        return self.start < interval.start
