import Interval

class Closed(Interval):
    def __init__(self, start = 0, end = 0):
        super().__init__(min(start, end), max(start, end))
