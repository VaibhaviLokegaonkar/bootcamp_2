import Interval

class SemiOpen(Interval):
    def __init__(self, start = 0, end = 0):
        super().__init__(min(start, end), max(start, end) - 1)

    def __repr__(self):
        return "[" + str(self.start - 1) + ", " + str(self.end + 1) + ")"